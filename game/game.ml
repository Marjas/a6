open Definitions
open Constants
open Util
open Player 
open Gamestate
open Ufo
open Bullet

(* TODO: change this *)
type game = Gamestate.game

let init_game () : game =
  {player1 = init_player Red;
   player2 = init_player Blue;
   ufolist = [];
   redbulletlist = [];
   bluebulletlist = [];
   powerlist = [];
   state = 0}

let handle_time game =
  time_step_gamestate game

let handle_action game col act =
  let player = match col with
  | Red -> (game.player1,Red)
  | Blue -> (game.player2,Blue) in
  match act with
  | Move lst -> (match (snd player) with
                | Red -> {game with player1 = change_moves (fst player) lst}
                | Blue -> {game with player2 = change_moves (fst player) lst})
  
  | Shoot (_,_,_) -> 
      let (lst,character) = (shoot (fst player) act) in
      (match (snd player) with
      | Red -> let new_list = game.redbulletlist @ lst in
               {game with player1 = character; redbulletlist = new_list}
      | Blue -> let new_list = game.bluebulletlist @ lst in
                {game with player2 = character; bluebulletlist = new_list})
  
  | Focus foc -> let new_player = change_focus (fst player) foc in 
                 (match (snd player) with
                 | Red -> {game with player1 = new_player}
                 | Blue -> {game with player2 = new_player})

  | Bomb -> let new_player = bomb (fst player) in
            match new_player with
            | Some character -> 
                (delete_all_bullets game.redbulletlist;
                delete_all_bullets game.bluebulletlist;
                let temp = {game with redbulletlist = [];
                bluebulletlist = []} in
                (match (snd player) with
                | Red -> {temp with player1 = character}
                | Blue -> {temp with player2 = character}))
            | None -> game 

let get_data (game:game): game_data =
  ((get_team_data game.player1), (get_team_data game.player2), 
   (List.fold_left (fun acc e -> (get_ufo e)::acc) [] game.ufolist),
   (game.redbulletlist @ game.bluebulletlist), game.powerlist)