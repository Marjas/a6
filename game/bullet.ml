open Definitions
open Constants
open Util
open Netgraphics

let shoot_bullets (a:action)(col:color)(p:position): bullet list =
  match a with
  | Shoot (t,pos,acc) -> 
    (match t with
    | Spread -> 
        (let rec get_spread_bullets (n : int) : bullet list = 
          (match n with
          | 0 -> []
          | _ -> 
           let m = cSPREAD_NUM - n in
           let idd = next_available_id () in
           add_update (AddBullet (idd, col, Spread, p));
           {b_type = Spread;
            b_id = idd;
            b_pos = p;
            b_vel = scale (float cSPREAD_SPEED) (rotate (unit_v (subt_v pos p)) 
              ((2. *. pi) /. (float (cSPREAD_NUM)) *. (float m)));
            b_accel = acc;
            b_radius = cSPREAD_RADIUS;
            b_color = col} :: (get_spread_bullets (n -1))) in
        get_spread_bullets cSPREAD_NUM)
    | Trail ->
        (let rec get_trail_bullets (n : int) : bullet list =
          let rec trail_help (m : int) : bullet list =
            (match m with
            | 0 -> []
            | _ -> let idd = next_available_id () in
                   add_update (AddBullet (idd, col, Trail, p));
                   {b_type = Trail;
                    b_id = idd;
                    b_pos = p;
                    b_vel = 
                    (match n with
                    | 1 -> scale ((float m) *. (float cTRAIL_SPEED_STEP)) 
                                  (unit_v (subt_v pos p))
                    | 2 -> scale ((float m) *. (float cTRAIL_SPEED_STEP)) 
                                  (rotate_deg (unit_v (subt_v pos p)) 
                                              (float cTRAIL_ANGLE))
                    | 3 -> scale ((float m) *. (float cTRAIL_SPEED_STEP)) 
                                  (rotate_deg (unit_v (subt_v pos p)) 
                                              (360. -. (float cTRAIL_ANGLE)))
                            | _ -> failwith "Should never happen");
                    b_accel = acc;
                    b_radius = cTRAIL_RADIUS;
                    b_color = col} :: (trail_help (m - 1)))in
        (match n with
        | 0 -> []
        | _ -> (trail_help cTRAIL_NUM) @ (get_trail_bullets (n - 1))) in
        get_trail_bullets 3)
    | Bubble -> 
        (let idd = next_available_id () in
        add_update (AddBullet (idd, col, Bubble, p));
        [{b_type = Bubble;
         b_id = idd;
         b_pos = p;
         b_vel = scale (float cBUBBLE_SPEED) (unit_v (subt_v pos p));
         b_accel = acc;
         b_radius = cBUBBLE_RADIUS;
         b_color = col}])
    | Power -> [])
  | _  -> []

let move_bullet (bul : bullet) : bullet option = 
   let (x, y) = add_v bul.b_pos bul.b_vel in
     if in_bounds (x, y) then
       (add_update (MoveBullet (bul.b_id, (x, y)));
        let vel = bul.b_vel in
        let acc = bul.b_accel in
        Some {bul with b_pos = (x, y); b_vel = (add_v vel acc)})
     else 
       (add_update (DeleteBullet (bul.b_id));
        None)

let move_all_bullets (lst : bullet list) : bullet list =
  List.fold_left (fun acc x -> let temp = move_bullet x in
                                match temp with
                                | None -> acc
                                | Some y -> y :: acc) [] lst

let delete_all_bullets (bullst : bullet list) : unit = 
  List.fold_left (fun acc x -> add_update(DeleteBullet (x.b_id))) () bullst