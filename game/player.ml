open Definitions
open Constants
open Util
open Bullet
open Netgraphics

type player_character = {
  lives: int;
  bombs: int;
  score: int;
  power: int;
  charge: int;
  moves: (direction * direction) list;
  mercy: int;
  bomb_inv: int;
  attributes: player_char
}

let init_player (col : color) : player_character = 
  let pos = 
    match col with
    | Red -> ((float(cBOARD_WIDTH))/.8.,(float(cBOARD_HEIGHT))/.2.)
    | Blue -> ((float(cBOARD_WIDTH))*.(7./.8.),(float(cBOARD_HEIGHT))/.2.) in
  let idd = next_available_id () in
  add_update (AddPlayer (idd, col, pos));
  add_update (SetCharge (col,0));
  add_update (SetPower (col,0));
  add_update (SetBombs (col,cINITIAL_BOMBS));
  add_update (SetScore (col,0));
  add_update (SetLives (col,cINITIAL_LIVES));
  {lives = cINITIAL_LIVES;
   bombs = cINITIAL_BOMBS;
   score = 0;
   power = 0;
   charge = 0;
   moves = [];
   mercy = 0;
   bomb_inv = 0;
   attributes = {p_id = idd; p_pos = pos; 
                 p_focused = false; p_radius = cHITBOX_RADIUS;
                 p_color = col}}

let die (a : player_character) : player_character = 
  let l = a.lives - 1 in
  let p = a.power / 2 in
  add_update(SetLives (a.attributes.p_color,l));
  add_update(SetBombs (a.attributes.p_color,cINITIAL_BOMBS));
  add_update(SetPower (a.attributes.p_color,p));
  {a with lives = l; power = p; bombs = cINITIAL_BOMBS; 
   moves = []; mercy = cINVINCIBLE_FRAMES}

(* Needs to be called on both players *)
let charge (a : player_character) : player_character =
  let ch = a.charge + a.power + cCHARGE_RATE in
  if ch > cCHARGE_MAX then 
  (add_update (SetCharge (a.attributes.p_color,cCHARGE_MAX));
    {a with charge = cCHARGE_MAX})
  else (add_update (SetCharge(a.attributes.p_color,ch));
    {a with charge = ch}) 

let point_award (a : player_character) (x : int) : player_character = 
  let points = a.score + x in
  add_update (SetScore (a.attributes.p_color,points));
  {a with score = points}

let add_power (a : player_character) : player_character = 
  let p = a.power + 1 in
  add_update (SetPower (a.attributes.p_color,p));
  {a with power = p}

let bomb (a : player_character) : player_character option =
  if a.bombs = 0 then None else
  (let b = a.bombs - 1 in
   add_update (UseBomb a.attributes.p_color);
   add_update (SetBombs (a.attributes.p_color, b));
   Some {a with bombs = b ; bomb_inv = cBOMB_DURATION})

let shoot (a : player_character) (act : action) = 
  match act with
  | Shoot (t,_,_) ->  
      let f = a.charge in
      let c = cost_of_bullet t in
      if f >= c
      then (add_update (SetCharge (a.attributes.p_color, (f - c)));
            (shoot_bullets act a.attributes.p_color a.attributes.p_pos),
            {a with charge = (f - c)})
      else ([],a)
  | _ -> ([], a)

let move (a : player_character): player_character =
  let move_player pos l =
    add_update (MovePlayer (a.attributes.p_id, pos));
    let att = a.attributes in
    {a with moves = l; attributes = {att with p_pos = pos}} in
  let mag = if a.attributes.p_focused then cFOCUSED_SPEED else cUNFOCUSED_SPEED in
  match a.moves with
  | x::xs -> 
      let vect = vector_of_dirs x (float mag) in
      let (x,y) = add_v a.attributes.p_pos vect in
      if in_bounds (x,y) then move_player (x,y) xs
      else if x > float cBOARD_WIDTH && y > float cBOARD_HEIGHT then 
      move_player (float cBOARD_WIDTH, float cBOARD_HEIGHT) xs
      else if x < 0. && y < 0. then move_player (0.,0.) xs
      else if x < 0. then move_player (0.,y) xs
      else if y < 0. then move_player (x,0.) xs
      else if x > float cBOARD_WIDTH then 
      move_player (float cBOARD_WIDTH, y) xs
      else move_player (x, float cBOARD_HEIGHT) xs
  | _ -> a

let change_focus (a: player_character) (foc:bool) : player_character=
  let att = a.attributes in
  {a with attributes = {att with p_focused = foc}}

let change_moves (a: player_character) (m: (direction * direction) list) =
  {a with moves = m}

let get_team_data (a: player_character): team_data =
  (a.lives,a.bombs,a.score,a.power,a.charge,a.attributes)

let time_step_player (a: player_character): player_character =
  let temp = move a in
  let inv = temp.mercy - 1 in
  let binv = temp.bomb_inv - 1 in
  let temp2 = if inv >= 0 then {temp with mercy = inv} else temp in
  if binv >= 0 then {temp2 with bomb_inv = binv} else (charge temp2)