open Constants
open Definitions
open Util

type player_character = {
  lives: int;
  bombs: int;
  score: int;
  power: int;
  charge: int;
  moves: (direction * direction) list;
  mercy: int;
  bomb_inv: int;
  attributes: player_char
}

(* Given color, initializes a player_character *)
val init_player : color -> player_character

(*Removes a life from a player_character. *)
val die : player_character -> player_character

(* Awards a certain amount of points to a player_character *)
val point_award : player_character -> int -> player_character

(* Adds power to a player_character *)
val add_power : player_character -> player_character

(* Returns None if there the given player has no bombs left. 
   Returns the same player_character with 1 fewer bombs otherwise. *)
val bomb : player_character -> player_character option

(* Creates bullets and returns a player_character with less charge *)
val shoot : player_character -> action -> bullet list * player_character

(* Changes the focus of the player *)
val change_focus : player_character -> bool -> player_character

(* Changes the move list that is given *)
val change_moves : player_character -> (direction * direction) list -> 
  player_character

(* Changes player_character to team_data *)
val get_team_data : player_character -> team_data

(* Performs all actions needed in each time step for a player *)
val time_step_player : player_character -> player_character