open Constants
open Definitions
open Ufo
open Player

type game = {
  player1 : player_character; 
  player2 : player_character;
  ufolist : myufo list;
  redbulletlist : bullet list;
  bluebulletlist : bullet list;
  powerlist : power list;
  state : int;
}

(* Performs all actions required in timestep as given by the writeup *)
val time_step_gamestate : game -> game * result