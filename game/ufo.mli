open Definitions
open Constants

type myufo = {
  id : int;
  pos : position;
  vel : velocity;
  radius : int;
  red_hits : int;
  blue_hits : int;
  time_steps : int
}

(*Creates a new object of type myufo*)
val create : unit -> myufo

(*Deletes ufo and spawns powerups on the screen. 
  Call when ufo has taken enough hits.*)
val delete_ufo : myufo -> position -> position -> power list

(*One time step for myufo. call at every time step.*)
val time_step_ufo : myufo -> myufo

(*Returns the ufo object corresponding to the 
  myufo object provided*)
val get_ufo : myufo -> ufo