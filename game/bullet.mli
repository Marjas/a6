open Definitions

(* Takes an action and color and creates initializes bullets and puts 
   them in a list *)
val shoot_bullets : action -> color -> position -> bullet list

(* Takes a bullet list, moves all bullets, and returns list of bullets in 
   bounds *)
val move_all_bullets : bullet list -> bullet list

(* Takes a bullet list and deletes them all from the gui *)
val delete_all_bullets : bullet list -> unit