open Definitions
open Constants
open Util
open Player 
open Netgraphics
open Ufo
open Bullet

type game = {
  player1 : player_character; 
  player2 : player_character;
  ufolist : myufo list;
  redbulletlist : bullet list;
  bluebulletlist : bullet list;
  powerlist : power list;
  state : int;
 }

let is_colliding (a:unit_type) (p1:position) (b:unit_type) (p2:position): bool =
  let get_radius (c:unit_type) : int =
    match c with
    | Player (_)   -> cHITBOX_RADIUS
    | Bullet (t,_) -> radius_of_bullet t
    | UFO          -> cUFO_RADIUS in
  if (distance p1 p2) > float ((get_radius a)+(get_radius b)) 
    then false else true

let check_bullet_player (player : player_character) (bul : bullet) : bool =
  is_colliding (Player (player.attributes.p_color)) 
               player.attributes.p_pos 
               (Bullet (bul.b_type, bul.b_color)) 
               bul.b_pos

let check_bulletlst_player (player : player_character) (bullst : bullet list) = 
  List.fold_left (fun acc x -> 
    if acc then acc
    else if (player.bomb_inv > 0 || player.mercy > 0) && 
            check_bullet_player player x then
      false
    else if check_bullet_player player x then
      true
    else false) false bullst

let is_grazing (bul : bullet) (player : player_character) : bool =
  if ((distance bul.b_pos player.attributes.p_pos) < 
       float (bul.b_radius + cGRAZE_RADIUS)) && 
     ((distance bul.b_pos player.attributes.p_pos) >  
       float (bul.b_radius + cHITBOX_RADIUS))
    then true 
  else false

(* First list is bullets grazing, second is not grazing *)
let graze_list (player : player_character) (bullst : bullet list) =
  List.fold_left (fun acc x -> 
    if is_grazing x player then (x :: (fst acc), (snd acc))
    else ((fst acc), x :: (snd acc))) ([], []) bullst

let play_graze (grazelist : bullet list) : unit = 
  if List.length grazelist > 0 then add_update (Graze)
  else ()

let graze_check (gam :game) : game =
  let redgrazing = graze_list gam.player1 gam.bluebulletlist in
  let bluegrazing = graze_list gam.player2 gam.redbulletlist in
  let temp = 
    if gam.player1.bomb_inv > 0 then
      (delete_all_bullets (fst redgrazing);
      {gam with bluebulletlist = (snd redgrazing)})
    else 
      (play_graze (fst redgrazing);
      let tempplayer = point_award gam.player1 
        (cGRAZE_POINTS * List.length (fst redgrazing)) in
      {gam with player1 = tempplayer}) in
  if temp.player2.bomb_inv > 0 then
    (delete_all_bullets (fst bluegrazing);
    {temp with redbulletlist = (snd bluegrazing)})
  else 
    (play_graze (fst bluegrazing);
    let tempplayer2 = point_award temp.player2 
      (cGRAZE_POINTS * List.length (fst bluegrazing)) in
    {temp with player2 = tempplayer2})


let final_bullet_check (gam : game) : game = 
  let redcheck = check_bulletlst_player gam.player1 gam.bluebulletlist in
  let bluecheck = check_bulletlst_player gam.player2 gam.redbulletlist in

  if (redcheck && bluecheck) then
      (
      delete_all_bullets gam.redbulletlist;
      delete_all_bullets gam.bluebulletlist;
      let newplayer1 = die gam.player1 in
      let newplayer2 = die gam.player2 in
      {gam with 
       player1 = point_award newplayer1 cKILL_POINTS;
       player2 = point_award newplayer2 cKILL_POINTS; 
       redbulletlist = []; 
       bluebulletlist = []})

  else if (redcheck && (not bluecheck)) then
       (
      delete_all_bullets gam.redbulletlist;
      delete_all_bullets gam.bluebulletlist;
      let newplayer1 = die gam.player1 in
      let newplayer2 = point_award gam.player2 cKILL_POINTS in
      {gam with 
       player1 = point_award newplayer1 cKILL_POINTS; 
       player2 = newplayer2; 
       redbulletlist = []; 
       bluebulletlist = []})

  else if ((not redcheck) && bluecheck) then
       (
      delete_all_bullets gam.redbulletlist;
      delete_all_bullets gam.bluebulletlist;      
      let newplayer1 = point_award gam.player1 cKILL_POINTS in
      let newplayer2 = die gam.player2 in
      {gam with 
       player1 = newplayer1; 
       player2 = point_award newplayer2 cKILL_POINTS; 
       redbulletlist = []; 
       bluebulletlist = []})

  else (* Both players are not hit *)
    gam

let check_bullet_ufo (ufoo : myufo) (bul : bullet) : bool =
  is_colliding UFO ufoo.pos (Bullet (bul.b_type, bul.b_color)) bul.b_pos

let check_bulletlst_ufo (ufoo : myufo) (bullst : bullet list) = 
  match bullst with
  | [] -> (ufoo, [])
  | x :: y -> 
    if x.b_color = Red then
      List.fold_left (fun acc x -> 
        if check_bullet_ufo (fst acc) x then 
          let temp = (fst acc).red_hits + 1 in
          ({(fst acc) with red_hits = temp}, x :: (snd acc))
        else acc) (ufoo, []) bullst
    else 
      List.fold_left (fun acc x -> 
        if check_bullet_ufo (fst acc) x then 
          let temp = (fst acc).blue_hits + 1 in
          ({(fst acc) with blue_hits = temp}, x :: (snd acc))
        else acc) (ufoo, []) bullst

let check_bullet_ufolst (ufolst : myufo list) (bullst : bullet list) =
  List.fold_left (fun acc x -> let temp = check_bulletlst_ufo x bullst in
    ((fst temp) :: (fst acc), (snd temp) @ (snd acc))) ([], []) ufolst 

let final_ufo_check (gam : game) : game =
  let temp = check_bullet_ufolst gam.ufolist gam.redbulletlist in
  let temp2 = check_bullet_ufolst (fst temp) gam.bluebulletlist in
  let real = List.fold_left (fun acc x -> 
    if (x.red_hits + x.blue_hits) > cUFO_HITS then
      ((fst acc), (snd acc) @ 
       (delete_ufo x gam.player1.attributes.p_pos gam.player2.attributes.p_pos))
    else (x :: (fst acc), (snd acc))) ([], gam.powerlist) (fst temp2) in
  let newredbullst = List.fold_left (fun acc x -> 
    if List.mem x (snd temp) then acc
    else x :: acc) [] gam.redbulletlist in
  let newbluebullst = List.fold_left (fun acc x -> 
    if List.mem x (snd temp2) then acc
    else x :: acc) [] gam.bluebulletlist in
  delete_all_bullets (snd temp);
  delete_all_bullets (snd temp2);
  {gam with 
   ufolist = (fst real); 
   powerlist = (snd real); 
   redbulletlist = newredbullst; 
   bluebulletlist = newbluebullst}

let check_player_power (player : player_character) (powerlst : power list) =
  List.fold_left (fun acc x -> 
    if check_bullet_player player x then 
      (add_update (DeleteBullet (x.b_id));
      (point_award (add_power (fst acc)) cPOWER_POINTS, (snd acc)))
    else ((fst acc), x :: (snd acc))) (player, []) powerlst

let final_power_check (gam : game) : game = 
  let newred = check_player_power gam.player1 gam.powerlist in
  let newblue = check_player_power gam.player2 (snd newred) in
  {gam with 
   player1 = (fst newred); 
   player2 = (fst newblue); 
   powerlist = (snd newblue)}

let add_ufo (gam : game) : game = 
  if gam.state mod cUFO_SPAWN_INTERVAL = 0 then
    let tempufo = gam.ufolist in
    {gam with ufolist = (create()) :: tempufo}
  else gam

let handle_no_die (gam : game) : result =
  if gam.player1.score > gam.player2.score then
    (add_update (GameOver (Winner (Red))); Winner (Red))
  else if gam.player1.score < gam.player2.score then
    (add_update (GameOver (Winner (Blue))); Winner (Blue))
  else
    (add_update(GameOver (Tie)); Tie)

let time_step_gamestate (gam : game) : game * result =
  let newredbullet = move_all_bullets gam.redbulletlist in
  let newbluebullet = move_all_bullets gam.bluebulletlist in
  let newpower = move_all_bullets gam.powerlist in
  let newplayer1 = time_step_player gam.player1 in
  let newplayer2 = time_step_player gam.player2 in
  let newufo = List.fold_left (fun acc x -> 
                                (time_step_ufo x) :: acc) [] gam.ufolist in
  let newstate = gam.state + 1 in
  let temp = {player1 = newplayer1; 
              player2 = newplayer2; 
              redbulletlist = newredbullet; 
              bluebulletlist = newbluebullet; 
              powerlist = newpower; 
              ufolist = newufo; 
              state = newstate} in
  let temp2 = add_ufo (final_power_check (final_bullet_check (
                graze_check (final_ufo_check temp)))) in
  if temp2.state = int_of_float (cTIME_LIMIT /. cUPDATE_TIME) then 
    (temp2, handle_no_die temp2)
  else if ((temp2.player1.lives = 0) && (temp2.player2.lives = 0)) then
    (temp2, handle_no_die temp2)
  else if temp2.player1.lives = 0 then
    (add_update (GameOver (Winner (Blue)));
    (temp2, Winner (Blue)))
  else if temp2.player2.lives = 0 then
    (add_update (GameOver (Winner (Red)));
    (temp2, Winner (Red)))
  else (* Game does not end *)
    (temp2, Unfinished)