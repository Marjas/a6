open Definitions
open Constants
open Util
open Netgraphics

type myufo = {
  id : int;
  pos : position;
  vel : velocity;
  radius : int;
  red_hits : int;
  blue_hits : int;
  time_steps : int
}

let create () : myufo =
  Random.init 43;
  let x = (0.25 *. (float cBOARD_WIDTH)) +. 
          (Random.float (0.5 *. (float cBOARD_WIDTH))) in
  let y = if (Random.int 2) = 0 then 0. else (float cBOARD_HEIGHT) in
  let p :position = (Random.float (float cBOARD_WIDTH) , 
                     Random.float (float cBOARD_HEIGHT)) in
  let vel :velocity = scale (float cUFO_SPEED) (unit_v (subt_v p (x,y))) in
  let idd = next_available_id () in
  add_update (AddUFO (idd, (x,y)));
  {id = idd;
   pos = (x,y);
   vel = vel;
   radius = cUFO_RADIUS;
   red_hits = 0;
   blue_hits = 0;
   time_steps = 0
  }

let change_vel (u:myufo) : myufo =
  let p :position = (Random.float (float cBOARD_WIDTH) , 
                     Random.float (float cBOARD_HEIGHT)) in
  let v :velocity = scale (float cUFO_SPEED) (unit_v (subt_v p u.pos)) in
  {u with vel = v}

let move (u:myufo) : myufo =
  let p :position = add_v u.pos u.vel in
  add_update (MoveUFO (u.id, u.pos));
  {u with pos = p}

let delete_ufo (u:myufo) (p_red:position) (p_blue:position): power list =
  add_update (DeleteUFO u.id);
  let rec create_power (p:position) (col:color) : power =
    let pos :position = (Random.float (float cBOARD_WIDTH) , 
                         Random.float (float cBOARD_HEIGHT)) in
    let un = unit_v (subt_v pos u.pos) in
    let s = Random.int cUFO_SCATTER_RADIUS in
    let np = scale (float s) un in
    let final_pos = add_v u.pos np in 
    if not (in_bounds final_pos) then create_power p col else
    let id = next_available_id() in
    add_update (AddBullet (id,col,Power,final_pos));
    {b_type = Power;
     b_id = id;
     b_pos = final_pos;
     b_vel =scale (float (speed_of_bullet Power)) (unit_v (subt_v p final_pos));
     b_accel = (0.,0.);
     b_radius = radius_of_bullet Power;
     b_color = col} in
  let total_hits = u.red_hits + u.blue_hits in
  let num_red = truncate (float(u.red_hits)/.
                float(total_hits)*.float(cUFO_POWER_NUM)) in
  let num_blue = cUFO_POWER_NUM - num_red in
  let rec get_power n p col =
    if n = 0 then [] else (create_power p col)::(get_power (n-1) p col) in
  (get_power num_red p_red Red) @ (get_power num_blue p_blue Blue)

let time_step_ufo (u:myufo) : myufo =
  let time = u.time_steps + 1 in
  let new_ufo = if (time mod cUFO_MOVE_INTERVAL) = 0 
  then  change_vel (move u) else move u in
  {new_ufo with time_steps = time}

let get_ufo (u:myufo) : ufo =
  {u_id = u.id;
   u_pos = u.pos;
   u_vel = u.vel;
   u_radius = u.radius;
   u_red_hits = u.red_hits;
   u_blue_hits = u.blue_hits
   }
