open Team
open Definitions
open Constants
open Util

(* [receive_data d] is called whenever a game update comes from the server.
 * It's up to you what to do with this update. *)
type gamedata = {
  red_data : team_data option;
  blue_data : team_data option;
  ufolist : ufo list;
  bulletlist : bullet list;
  powerlist : power list
}

let data : gamedata ref = ref {
  red_data = None;
  blue_data = None;
  ufolist = [];
  bulletlist = [];
  powerlist = [];
}

let receive_data (d : game_data) : unit =
  match d with
   | (a, b, c, d, e) ->
     data := {
       red_data = Some a;
       blue_data = Some b;
       ufolist = c;
       bulletlist = d;
       powerlist = e;
     }; ()

let () = Random.self_init ()

let count = ref 0
let state = ref 0

let rand_direction () = let roll = Random.int 8 in
  if roll = 0 then (North, Neutral)
  else if roll = 1 then (North, East)
  else if roll = 2 then (East, Neutral)
  else if roll = 3 then (East, South)
  else if roll = 4 then (South, Neutral)
  else if roll = 5 then (South, West)
  else if roll = 6 then (West, Neutral)
  else (West, North)
  
let rand_loc () =
  let x = Random.float 600. in
  let y = Random.float 600. in
  (x, y)

let rand_accel () =
  let angle = Random.float 360. in
  let magn = Random.float cACCEL_LIMIT in
  (magn *. (cos angle), magn *. (sin angle))

let shoot (c:color) (t:bullet_type) =
  let (p1,p2) = if c = Blue then ((!data).red_data,(!data).blue_data) 
  else ((!data).blue_data,(!data).red_data) in
  let pos = match p1 with
            |None -> rand_loc ()
            |Some a -> match a with
                       |(_,_,_,_,_,ch) -> ch.p_pos in
  let acc = match p2 with
            | None -> (0.,0.)
            | Some a -> match a with
                        |(_,_,_,_,_,ch) -> scale cACCEL_LIMIT 
                                           (unit_v (subt_v pos (ch.p_pos))) in
  send_action (Shoot (t, pos, acc))

let getbombs (c:color) : int =
  match c with 
  | Red -> (match (!data).blue_data with
           | Some (_,x,_,_,_,_) -> x
           | None -> 0)
  | Blue -> (match (!data).red_data with
           | Some (_,x,_,_,_,_) -> x
           | None -> 0)

let getcharge (c:color) : int =
  match c with
  | Red -> (match (!data).red_data with
           | Some (_,_,_,_,x,_) -> x
           | None -> 0) 
  | Blue -> (match (!data).blue_data with
           | Some(_,_,_,_,x,_) -> x
           | None -> 0)

let getposition (c:color) : position =
  match c with
  | Red -> (match (!data).red_data with
           | Some (_,_,_,_,_,x) -> x.p_pos
           | None -> (0., 0.)) 
  | Blue -> (match (!data).blue_data with
           | Some (_,_,_,_,_,x) -> x.p_pos
           | None -> (0., 0.))

let bulletcheck (c : color) (bullst : bullet list) : bool =
  let check (bul : bullet) (p : position) : bool =
    if ((distance bul.b_pos p) < float (bul.b_radius + cGRAZE_RADIUS))
      then true
    else false in
  
  match c with
  | Red -> let pos = getposition Red in
           List.fold_left (fun acc x -> if acc then acc
                                        else if (x.b_color = Blue) then
                                          check x pos
                                        else false) false bullst
  | Blue -> let pos = getposition Blue in
            List.fold_left (fun acc x -> if acc then acc
                                        else if (x.b_color = Red) then
                                          check x pos
                                        else false) false bullst

let bulletcheck2 (c : color) (bullst : bullet list) : bullet list =
  let check (bul : bullet) (p : position) : bool =
    if ((distance bul.b_pos p) < float (bul.b_radius + cGRAZE_RADIUS))
      then true
    else false in
  
  match c with
  | Red -> let pos = getposition Red in
           List.fold_left (fun acc x -> if (x.b_color = Blue && check x pos) then
                                          x :: acc
                                        else acc) [] bullst
  | Blue -> let pos = getposition Blue in
            List.fold_left (fun acc x -> if (x.b_color = Red && check x pos) then
                                          x :: acc
                                        else acc) [] bullst

let dodge (c : color) (bullst : bullet list) : direction = 
  if bulletcheck c bullst then 
    let avoid = bulletcheck2 c bullst in
    let redpos = getposition Red in
    let bluepos = getposition Blue in
    match c with
    | Red -> 
    | Blue ->
  else let roll = Random.int 8 in
  if roll = 0 then (North, Neutral)
  else if roll = 1 then (North, East)
  else if roll = 2 then (East, Neutral)
  else if roll = 3 then (East, South)
  else if roll = 4 then (South, Neutral)
  else if roll = 5 then (South, West)
  else if roll = 6 then (West, Neutral)
  else (West, North)

let bot c =
  while true do
  let charge = getcharge c in
  let bomb = getbombs c in
  let d = rand_direction () in
  let () = send_action (Move [d;d;d;d]) in
  let () = if (bomb > 0 && charge > cBUBBLE_COST) 
             then shoot c Bubble else () in
  let () = if bomb = 0 
             then shoot c Spread else () in 
  let () = if bulletcheck c (!data).bulletlist then 
             (state := 0; send_action(Bomb)) else () in
  incr count;
  Thread.delay 0.20
done

let () = start_bot bot receive_data